# How to run

1. Update inv.ini with required host/ip and ansible.cfg with username if needed
1. Run ansible command
```sh
ansible-playbook play.yaml -k -K
```
